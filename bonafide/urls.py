from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
urlpatterns = [
    url('^admin/', admin.site.urls),
    url('^certificate/',include('certificate.urls')),
    url('^logout/$', auth_views.logout),
    url('^$', auth_views.login, {'template_name': 'certificate/login.html'},name = 'login')
]
