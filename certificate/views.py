from django.shortcuts import render,redirect
from certificate.models import dbUser
from django.http import HttpResponse
from django.views.generic import View
from django.template import Context
from .utils import render_to_pdf 
from .forms import UserRegistrationForm

class GeneratePdf(View):
	def get(self, request, *args, **kwargs):
		user = dbUser.objects.get(username= request.user)
		context = Context({"name": user.name,"fathers_name": user.fathers_name,"year": user.year,"gender": user.gender,
		})
		pdf = render_to_pdf('certificate/certificate.html', context)
		if pdf:
			return pdf
		return HttpResponse("Not found")

def index(request):
    user = dbUser.objects.get(username= request.user)
    context = {"name": user.name,"fathers_name": user.fathers_name,"year": user.year,"gender": user.gender,'course':user.course}
    return render(request,'certificate/certificate.html', context)

def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
            
    else:
        form = UserRegistrationForm()
    return render(request, 'certificate/reg.html', {'form' : form})


