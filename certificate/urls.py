from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views
urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^register/$', views.register , name='register'),
    url(r'^certificate/$',views.GeneratePdf.as_view(),name='GeneratePdf'),
    ]