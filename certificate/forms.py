from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.admin.widgets import AdminDateWidget

class UserRegistrationForm(UserCreationForm):
	email = forms.CharField(
	label = 'Email',
	max_length = 32,
	)
	fathers_name = forms.CharField(
	label = 'fathers_name',
	max_length = 32,)
	year = forms.IntegerField(
	label = 'year',)
	course = forms.CharField(
	label = 'course',
	max_length = 32,)
	dob = forms.DateField(
	label = 'dob',)
	board = forms.CharField(
	label = 'board',
	max_length = 32,)
	CHOICES = (('Male', 'Male'),('Female', 'Female'),)
	gender = forms.ChoiceField(choices=CHOICES,
	label = 'gender',)

	class Meta:
		model = User
		fields = ['username','email','password1','password2','fathers_name','year','course','dob','board','gender']	


