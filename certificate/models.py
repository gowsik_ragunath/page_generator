from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
import datetime

class dbUser(models.Model):
	username = models.OneToOneField(User, on_delete=models.CASCADE)
	name = models.CharField(max_length=25,null = True)
	fathers_name = models.CharField(max_length=25,null = True)
	year = models.IntegerField(null = True)
	course = models.CharField(max_length = 30,null = True)
	dob = models.DateField(default = datetime.date.today)
	board = models.CharField(max_length=20,null = True)
	cur_date = models.DateField(default = datetime.date.today)
	gender = models.CharField(max_length=7,null = True)
	email = models.EmailField(max_length=20,null = True)

	def __str__(self):
	    return self.name