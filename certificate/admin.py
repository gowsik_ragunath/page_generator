from django.contrib import admin

# Register your models here.
from .models import dbUser

admin.site.register(dbUser)
